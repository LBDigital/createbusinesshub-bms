<?php
	session_start();
	if ( isset($_SESSION['authed']) && $_SESSION['authed'] === true ){
		header("location: ../");
		exit;
	}


	require '../vendor/autoload.php';


	$form_err = "";

	// check if form data has been submitted
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){

		// validate username & password
		if (!isset($_POST['username']) || !isset($_POST['password'])){
			$form_err = "All fields are required";
		}else if (empty(trim($_POST['username'])) || empty(trim($_POST['password']))){
			$form_err = "All fields are required";
		}else{
			$username = strtolower( trim( $_POST['username'] ) );
			$password = trim($_POST['password']);
		}

		// validate credentials
		if (empty($form_err)){

			// get user from DB by username
			$eav = $marshaler->marshalJson(json_encode([
				':tenant_username' => 'TENANT_' . $username,
				':admin_username' => 'ADMIN_' . $username
			]));

			$params = [
				'TableName' => DB_TABLE,
				'FilterExpression' => '#id IN (:tenant_username, :admin_username)',
				'ExpressionAttributeNames' => [ '#id'=>'id' ],
				'ExpressionAttributeValues' => $eav
			];


			try{
				$result = $dynamoDb->scan($params);

			}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
				echo "Unable to query:\n";
				echo $e->getMessage() . "\n";
			}


			if ($result['Count'] < 1){
				$form_err = "Username not registered!";
			}else{
				$user = $marshaler->unmarshalItem($result['Items'][0]);
				if (password_verify($password, $user['password'])){
					$_SESSION['authed'] = true;
					$_SESSION['id'] = $user['id'];
					$_SESSION['name'] = $user['name'];
					$_SESSION['email'] = $user['email'];
					if (substr($user['id'], 0, 5) === "ADMIN"){
						$_SESSION['admin'] = true;
					}

					header('Location: ../');

				}else{
					$form_err = "Incorrect password!";
				}
			}


		}

	}


 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<?php include '../partials/head.php' ?>
		<title>CREATE Bookings - Login</title>
	</head>
	<body>
		<header>
			<?php include '../partials/header.php'; ?>
		</header>

		<main class="pt-4">

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 col-lg-4">

						<div class="bg-light p-4 mb-2">
							<h2 class="text-center mb-4">LOGIN</h2>

							<form action="" method="post">
								<div class="form-group">
									<!-- <label for="username_input">Username</label> -->
									<input type="text" name="username" placeholder="Username" class="form-control" id="username_input" autocomplete="username" autofocus>
									<!-- <small id="username_help" class="form-text text-muted"></small> -->
								</div>
								<div class="form-group mb-0">
									<!-- <label for="password_input">Password</label> -->
									<input type="password" name="password" placeholder="Password" class="form-control" id="password_input" autocomplete="current-password">
								</div>

								<button class="btn btn-primary w-100 mt-4">Login</button>

								<small class="text-danger d-block mt-2"><?php echo $form_err; ?></small>
							</form>
						</div>

						<small class="text-muted">
							Please Note: This is an internal system, solely for
							use by tenants of "CREATE Business Hub". Current
							tenants can request access to this site from reception.
						</small>

					</div>
				</div>


			</div>

		</main>

		<footer>
			<?php include '../partials/footer.php'; ?>
		</footer>

	</body>
</html>
