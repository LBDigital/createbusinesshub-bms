<?php
	session_start();
	if ( isset($_SESSION['authed']) && $_SESSION['authed'] === true ){
		header("location: ../");
		exit;
	}

	require '../vendor/autoload.php';

	$form_err = "";

	// get verification code from url
	$verify = $_GET['v'];

	// get user from db by verification code
	$scanParams = [
		"TableName"=>DB_TABLE,
		"FilterExpression"=>"#v = :v",
		"ExpressionAttributeNames"=>[
			"#v"=>"verify"
		],
		"ExpressionAttributeValues"=>$marshaler->marshalJson(json_encode([
			":v"=>$verify
		]))
	];

	try {
		$result = $dynamoDb->scan( $scanParams );
	} catch (Aws\DynamoDb\Exception\DynamoDbException $e) {
		echo "Unable to get user:\n";
		echo $e->getMessage();
	}

	if (isset($result)){
		if ($result['Count'] > 0){
			// found user for verification code
			$tenant = $result['Items'][0];

			// if POST, save password
			if ($_SERVER['REQUEST_METHOD'] == 'POST'){

				// verify inputs
				if (
					!isset($_POST['password']) ||
					!isset($_POST['password_confirm']) ||

					empty(trim($_POST['password'])) ||
					empty(trim($_POST['password_confirm']))
				){
					$form_err = "All fields are required";

				} else if ($_POST['password'] != $_POST['password_confirm']){
					$form_err = "Passwords do not match";

				}else{
					// valid inputs, store to db and delete verify code
					$hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
					$updateItemParams = [
						"TableName" => DB_TABLE,
						"Key" => [
							"id"=>$tenant['id']
						],
						"UpdateExpression" => "SET #p = :p REMOVE #v",
						"ExpressionAttributeNames"=>[
							"#p"=>"password",
							"#v"=>"verify"
						],
						"ExpressionAttributeValues"=>$marshaler->marshalJson(json_encode([
							":p"=>$hash
						]))
					];

					try {
						$result = $dynamoDb->updateItem( $updateItemParams );
						// tenant updated succesfully
						$tenantUpdated = true;

					} catch (Aws\DynamoDb\Exception\DynamoDbException $e) {
						echo "Unable to update item:\n";
						echo $e->getMessage() . "\n";
					}

				}

			}

		}else{
			$verify_err = "Invalid verification code.  If you belive this is in error, please contact reception.";
		}
	}else{
		$verify_err = "An unknown error occured, please contact reception";
	}


 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<?php include '../partials/head.php' ?>
		<title>CREATE Bookings - Register</title>

		<?php if (isset($tenantUpdated)){
			// tenant has been updated, post to login.php with credentials
			$username = explode("_", $tenant['id']['S'])[1];
			?>
			<form action="./login.php" method="post" id="loginForm">
				<input type="hidden" name="username" value="<?php echo $username; ?>">
				<input type="hidden" name="password" value="<?php echo $_POST['password']; ?>">
			</form>
			<script type="text/javascript">
				// instantly submit the form
				document.querySelector('#loginForm').submit();
			</script>
		<?php } ?>
	</head>
	<body>
		<header>
			<?php include '../partials/header.php'; ?>
		</header>

		<main class="pt-4">

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 col-lg-4">

						<div class="bg-light p-4 mb-2">
							<h2 class="text-center mb-4">REGISTER</h2>

							<?php if (!isset($verify_err)) { ?>
								<form action="" method="post">
									<div class="form-group">
										<label for="passwordInput">Password</label>
										<input type="password" name="password" class="form-control" id="passwordInput" autofocus required>
										<small class="form-text text-muted">Create a password for your account</small>
									</div>
									<div class="form-group">
										<label for="confirmPasswordInput">Confirm Password</label>
										<input type="password" name="password_confirm" class="form-control" id="confirmPasswordInput" required>
										<small class="form-text text-muted">Repeat the same password as above</small>
									</div>

									<button class="btn btn-primary w-100 mt-4">Register</button>

									<small class="text-danger d-block mt-2"><?php echo $form_err; ?></small>
								</form>
							<?php }else{ ?>
								<p><?php echo $verify_err; ?></p>

								<a href="<?php echo BASE_DIR; ?>"/>
									<button class="btn btn-primary w-100 mt-4">
										Return to Safety
									</button>
								</a>
							<?php } ?>
						</div>

						<small class="text-muted">
							Create a password to finalise your registration and
							begin using the CBH meeting room booking system
							straight away
						</small>

					</div>
				</div>


			</div>

		</main>

		<footer>
			<?php include '../partials/footer.php'; ?>
		</footer>

	</body>
</html>
