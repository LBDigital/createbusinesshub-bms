<?php
	require '../vendor/autoload.php';


	session_start();
	if ( !isset($_SESSION['authed']) || $_SESSION['authed'] !== true){
		header("location: ../auth/login.php");
		exit;
	}

	$form_err = "";

	// check if form data has been submitted
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){

		// validate inputs
		if (
			!isset($_POST['room']) ||
			!isset($_POST['time']) ||

			empty(trim($_POST['room'])) ||
			empty(trim($_POST['time']))
		){
			$form_err = "Please specify a room, date and time for the booking";

		}else if (isset($_SESSION['admin']) && (
			!isset($_POST['user']) ||
			empty(trim($_POST['user']))
		)){
			$form_err = "Please choose who the booking is for";

		}else if ($_POST['room'] < 1 || $_POST['room'] > 3){
			$form_err = "Invalid room number";

		}else if (
			!isset($_SESSION['admin']) && // not admin
			( // not agreed to quota
				!isset($_POST['quotaAgree']) ||
				$_POST['quotaAgree'] != "on"
			)
		){
			$form_err = "You must agree to the quota terms";

		}else{
			// valid inputs
			$room = intval(trim($_POST['room']));
			$time = intval(trim($_POST['time']));
			$user = (isset($_SESSION['admin']) ? $_POST['user'] : $_SESSION['id']);
			$boardRequired = (
				isset($_POST['boardRequired']) &&
				$_POST['boardRequired'] == "on"
			);
			$quotaAgree = (
				isset($_SESSION['admin']) || // no quota if user is admin
				(
					isset($_POST['quotaAgree']) &&
					$_POST['quotaAgree'] == "on"
				)
			);
			$notes = $_POST['notes'];
			$bookingId = "BOOKING_" . uniqid();

			$bookingData = [
				'id' => $bookingId,
				'room' => $room,
				'time' => $time,
				'user' => $user,
				'board' => $boardRequired,
				'notes' => $notes
			];


			// ensure chosen time is still available for chosen room
			$scanParams = [
				'TableName' => DB_TABLE,
				'FilterExpression' => '#time = :t and #room IN (:3, :r)',
				'ExpressionAttributeNames' => [ '#time'=>'time', '#room'=>'room' ],
				'ExpressionAttributeValues' => $marshaler->marshalJson(json_encode([
					':t' => $time,
					':3' => 3,
					':r' => $room
				]))
			];

			try{
				$results = $dynamoDb->scan($scanParams);
			}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
				$form_err = "An unknown error has occured, please contact a system admin";
			}

			if ($results['Count'] > 0){
				// booking slot unavailable
				$form_err = "Sorry, chosen room is no longer available at chosen time";

			}else{
				// booking slot still available

				$putItemParams = [
					'TableName' => DB_TABLE,
					'Item' => $marshaler->marshalJson(json_encode($bookingData + [
						'createdAt' => time()
					]))
				];

				try{
					$GLOBALS['dynamoDb']->putItem($putItemParams);
					$roomBooked = true;
				}catch (Aws\DynamoDb\Exception\DynamoDbException $e){
					$form_err = "An unknown error occured, please contact a system admin";
				}


				if (isset($roomBooked)){
					// log action
					logger('BOOKING_CREATED', $_SESSION['id'],  $bookingData);

					if ($user == $_SESSION['id']){
						$email = $_SESSION['email'];
					}else{
						// get booking users email from db as admin is booking on behalf of a tenant
						


					}

					// send email to tenant
					$emailParams = new \SendGrid\Mail\Mail();
					$emailParams->setFrom("confirm@createbusinesshub.com", "Create Business Hub");
					$emailParams->setSubject("Booking Confirmation");
					$emailParams->addTo($_SESSION['email'], $_SESSION['name']);

					$bookingStr =
						($room == 3 ? "rooms 1 & 2" : "room ".$room).
						" on ".
						date("D jS F Y", $time*60*60).
						" at ".
						date("G", $time*60*60).":00";


					$extraNotesStr = "";
					if ($boardRequired || $notes){
						$extraNotesStr = "You ";
						if ($boardRequired){
							$extraNotesStr .= "have specified that you will need to connect your device to the interactive smart board";
							if ($notes){
								$extraNotesStr .= " and also ";
							}
						}
						if ($notes){
							$extraNotesStr .= "included the following notes with your booking: '".$notes."'";
						}
						$extraNotesStr .= ".";
					}

					$emailParams->addContent("text/plain",
						"You have succesfully booked meeting ".$bookingStr.".  You can make changes to or cancel this booking from your account panel.  ".$extraNotesStr
					);
					// $email->addContent("text/html", "");

					$sendgrid = new \SendGrid( SENDGRID_API_KEY );
					try {
						$response = $sendgrid->send( $emailParams );
					} catch (Exception $e) {
						// echo "Caught Exception: ". $e->getMessage() ."\n";
					}


				}

			}


		}

	}

 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<?php include '../partials/head.php' ?>

		<script src="../assets/js/booking-new.js" defer charset="utf-8"></script>
	</head>
	<body>

		<header>
			<?php include '../partials/header.php'; ?>
		</header>


		<main class="pt-4 new-booking">

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">

						<section class="show bg-light p-4 mb-4 text-center book-room-header">
							<h2 class="font-weight-bold mb-4">Book a Room</h2>
							<p class="text-primary">HIDE DATES THAT'VE ALREADY PASSED IN THIS MONTH; MINUS 1 from quota; fix tenant create checking for existing user; get tenant email for booking confirmation to send email</p>

							<?php if (isset($roomBooked)){ ?>

								<p>You have succesfully booked meeting <?php echo $bookingStr; ?>.  A confirmation email has been sent to '<?php echo $email; ?>'</p>

							<?php }else{ ?>

								<p>
									See below our available meeting rooms at CREATE
									Business Hub.  Choose a room to get started.
								</p>
								<small class="text-muted">
									NOTE: Both rooms can be merged into 1 large
									meeting room, simply select both rooms below.
								</small>

								<small class="text-danger d-block mt-2"><?php echo $form_err; ?></small>


							<?php } ?>

						</section>

						<?php if (!isset($roomBooked)){ ?>
							<div class="row mb-4">
								<div class="col">
									<section class="show room-choose" data-room="1">
										<div class="bg-light">
											<div class="room-img-container">
												<img src="../assets/images/room-1.jpg" class="w-100" alt="Room 1">
												<span>1</span>
											</div>
											<div class="room-tags">
												<span class="badge badge-pill badge-dark">Capacity: 12</span>
												<span class="badge badge-pill badge-dark">Smart Board w/HDMI</span>
												<span class="badge badge-pill badge-dark">Other</span>
												<span class="badge badge-pill badge-dark">Great</span>
												<span class="badge badge-pill badge-dark">Features</span>
												<span class="badge badge-pill badge-dark">Listed</span>
												<span class="badge badge-pill badge-dark">Here</span>
											</div>
										</div>
									</section>
								</div>
								<div class="col">
									<section class="show room-choose" data-room="2">
										<div class="bg-light">
											<div class="room-img-container">
												<img src="../assets/images/room-2.jpg" class="w-100" alt="Room 2">
												<span>2</span>
											</div>
											<div class="room-tags">
												<span class="badge badge-pill badge-dark">Capacity: 8</span>
												<span class="badge badge-pill badge-dark">This</span>
												<span class="badge badge-pill badge-dark">Room</span>
												<span class="badge badge-pill badge-dark">Has</span>
												<span class="badge badge-pill badge-dark">Great</span>
												<span class="badge badge-pill badge-dark">Features</span>
												<span class="badge badge-pill badge-dark">Too!</span>
											</div>
										</div>
									</section>
								</div>
							</div>
						<?php } ?>


						<div class="row mb-4">

							<div class="col-8">

								<section class="calendar bg-light" id="calendar">
									<div class="calendar-head">
										<div class="calendar-month">
											<span class="calendar-month-change disabled" id="prevMonth">
												<i class="fas fa-chevron-left"></i>
											</span>
											<span class="calendar-title" id="calendarTitle"></span>
											<span class="calendar-month-change" id="nextMonth">
												<i class="fas fa-chevron-right"></i>
											</span>
										</div>
									</div>
									<div class="calendar-days">
										<div class="calendar-day">
											<div class="calendar-header">MON</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">TUE</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">WED</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">THU</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">FRI</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">SAT</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
										<div class="calendar-day">
											<div class="calendar-header">SUN</div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
											<div class="calendar-date"></div>
										</div>
									</div>
								</section>

							</div>
							<div class="col">
								<section class="bg-light p-2 choose-time" id="chooseTime">
									<h4 class="text-center mb-2">Choose Time</h4>
									<div id="times" class="times-container"></div>
								</section>

								<section class="mt-4 choose-user" id="chooseUser">
									<select name="user" class="custom-select" id="chooseUserSelect">
										<?php
											if (isset($_SESSION['admin'])){
												// allow user to be chosen
												require '../scripts/get-users.php';
												$users = getUsers();

												echo '<option id="chooseUserOption" disabled selected>Who is the Booking for?</option>';

												foreach($users as $user){
													$userType = explode('_', $user['id'])[0];
													$userType = ucwords(strtolower($userType));
													$displayStr = $user['name']." (".$userType.")";

													echo '<option value="'.$user['id'].'">'.$displayStr.'</option>';
												}
											}
										?>
									</select>
								</section>
							</div>
						</div>


						<section class="complete-booking bg-light p-4 mb-4" id="completeBooking">
							<h3 class="font-weight-bold mb-3">Complete Booking</h3>

							<form action="" method="post">
								<input type="hidden" name="room" id="roomInput">
								<input type="hidden" name="time" id="timeInput">
								<input type="hidden" name="user" id="userInput">

								<div class="form-check mb-3">
									<input type="checkbox" name="boardRequired" class="form-check-input" id="boardCheckbox">
									<label for="boardCheckbox" class="form-check-label">
										I will need to connect my device to the interactive
										smart board for presentation.
									</label>
								</div>

								<div class="form-group mb-3">
									<label for="notesInput">Booking Notes</label>
									<textarea name="notes" id="notesInput" class="form-control" placeholder="Any special requirements?"></textarea>
									<small class="form-text text-muted">
										Specify any special requirements for the booking
										e.g: white board, extra chairs
									</small>
								</div>

								<?php if (!isset($_SESSION['admin'])){
									require '../scripts/quota-check.php'; ?>

									<div class="form-check mb-3">
										<input type="checkbox" name="quotaAgree" class="form-check-input" id="quotaCheckbox">
										<label for="quotaCheckbox" class="form-check-label">
											<?php
												$quota = quotaCheck();

												$labelStr = "I understand that ";
												if ($quota['bookings'] >= $quota['total']){
													// making this booking will exceed quota
													$labelStr .=
														"this booking will exceed my monthly quota
														of ".$quota['total']." free bookings since
														I have made ".$quota['bookings']." bookings
														so far this month and I will be invoiced
														£25<sup>+VAT</sup> at the end of the month.";

												}else{
													// this booking is within users quota
													$remaining = $quota['total']-$quota['bookings']-1;
													$labelStr .=
														"this booking counts towards my monthly
														bookings quota of ".$quota['total'].
														" bookings, meaning I will have ".$remaining.
														" free bookings remaining this month.";

												}
												echo $labelStr;
											?>
										</label>
									</div>
								<?php } ?>

								<button class="btn btn-primary w-100" id="completeBtn"
									<?php if(!isset($_SESSION['admin'])){ echo " disabled"; } ?> >
									Book meeting room
									<span class="font-weight-bold" id="confirmRoom"></span>
									on
									<span class="font-weight-bold" id="confirmDate"></span>
									at
									<span class="font-weight-bold" id="confirmTime"></span>
									<?php if (isset($_SESSION['admin'])){ echo " for "; } ?>
									<span class="font-weight-bold" id="confirmUser"></span>
								</button>

							</form>

							<small class="text-muted">
								By making this booking you agree too...
							</small>
						</section>


					</div>
				</div>
			</div>

		</main>


		<footer>
			<?php include '../partials/footer.php'; ?>
		</footer>


	</body>
</html>


<!--
TODO:
	- persistant sessions (just store session id in db with their user record??  - have a google, see whats simplest)
	- footer with lb.digital link NEED TO MAKE SURE LB DIGITAL IS SOMETHING BY THEN
 -->
