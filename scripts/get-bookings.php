<?php

require '../vendor/autoload.php';

session_start();
if ( !isset($_SESSION['authed']) || $_SESSION['authed'] !== true){
	http_response_code(401);
	exit;
}


$dateStr = explode('-', $_GET['date']);
$day = intval($dateStr[0]);
$month = intval($dateStr[1]);
$year = intval($dateStr[2]);

// get timestamp of first minute of day
$dayStart = mktime(0, 0, 0, $month, $day, $year);
// get timestamp of last minute of day
$dayEnd = mktime(23, 59, 59, $month, $day, $year);

// convert timestamps to hourcodes
$dayStart = floor($dayStart/60/60);
$dayEnd = floor($dayEnd/60/60);

// get all bookings for given date
$scanParams = [
	'TableName' => DB_TABLE,
	'FilterExpression' => 'begins_with(#id, :b) and #time between :start and :end',
	'ExpressionAttributeNames' => [ '#id'=>'id', '#time'=>'time' ],
	'ExpressionAttributeValues' => $marshaler->marshalJson(json_encode([
		':b' => 'BOOKING',
		':start' => $dayStart,
		':end' => $dayEnd
	]))
];

try{
	$result = $dynamoDb->scan($scanParams);

}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
	http_response_code(500);
	echo "error_getting_bookings_from_db";
	exit;
}


$bookings = array_map(function($booking){
	// parse booking
	$booking = $GLOBALS['marshaler']->unmarshalItem($booking);

	// add day number
	$ts = $booking['time']*60*60;
	$booking['hour'] = intval(date('G', $ts));

	return $booking;
}, $result['Items']);

echo json_encode($bookings);



?>
