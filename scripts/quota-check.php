<?php

require '../vendor/autoload.php';


function quotaCheck(){
	$userId = $_SESSION['id'];

	// get timestamp for start of month
	$monthStart = mktime(0, 0, 0, date("n"), 1);

	// get all bookings for user this month
	$bookingScanParams = [
		'TableName' => DB_TABLE,
		'FilterExpression' => '#user = :userid and #createdAt > :monthstart',
		'ExpressionAttributeNames' => [ '#user'=>'user', '#createdAt'=>'createdAt' ],
		'ExpressionAttributeValues' => $GLOBALS['marshaler']->marshalJson(json_encode([
			':userid' => $userId,
			':monthstart' => $monthStart
		]))
	];

	try{
		$bookingResult = $GLOBALS['dynamoDb']->scan($bookingScanParams);
	}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
		return false;
	}

	$bookings = $bookingResult['Count'];

	// get users booking quota
	$userScanParams = [
		'TableName' => DB_TABLE,
		'FilterExpression' => '#id = :id',
		'ExpressionAttributeNames' => [ '#id'=>'id' ],
		'ExpressionAttributeValues' => $GLOBALS['marshaler']->marshalJson(json_encode([
			':id' => $userId
		]))
	];

	try{
		$userResult = $GLOBALS['dynamoDb']->scan($userScanParams);
	}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
		return false;
	}

	$user = $GLOBALS['marshaler']->unmarshalItem($userResult['Items'][0]);
	$quota = $user['quota'];

	return [
		'bookings' => $bookings,
		'total' => $quota
	];

}

?>
