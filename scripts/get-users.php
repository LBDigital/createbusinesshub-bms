<?php

require '../vendor/autoload.php';

function getUsers(){
	$scanParams = [
		'TableName' => DB_TABLE,
		'FilterExpression' => 'begins_with(#id, :a) or begins_with(#id, :t) or begins_with(#id, :g)',
		'ExpressionAttributeNames' => [ '#id'=>'id' ],
		'ExpressionAttributeValues' => $GLOBALS['marshaler']->marshalJson(json_encode([
			':a' => 'ADMIN_',
			':t' => 'TENANT_',
			':g' => 'GUEST_'
		]))
	];

	try{
		$result = $GLOBALS['dynamoDb']->scan($scanParams);
	}catch(Aws\DynamoDb\Exception\DynamoDbException $e){
		return false;
	}

	$users = array_map(function($user){
		$user = $GLOBALS['marshaler']->unmarshalItem($user);

		return $user;

	}, $result['Items']);

	return $users;

}

?>
