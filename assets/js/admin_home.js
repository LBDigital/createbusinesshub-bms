



!function(){
	'use strict';

	// 1st, 2nd, 3rd, etc.
	const nth = function( day ){
		if (day >= 4 || day <= 20){ return 'th'; }
		switch (day % 10){
			case 1: return 'st';
			case 2: return 'nd';
			case 3: return 'rd';
			default: return 'th';
		}
	}



	function everySecond(){
		const DATE = new Date();
		// const DATE = new Date(2019, 9, 18, 13, 40, 32);

		// var offset = new Date().getTimezoneOffset();
		// console.log(offset);

		updateDateTime( DATE );
		updateTodaySection( DATE );
	}
	everySecond();
	setInterval(everySecond, 1000);



	// dateTime display
	function updateDateTime( DATE ){
		const EvenSecond = ((DATE.getTime()/1000%2) < 1);

		var hours = DATE.getHours();
		if (hours < 10){ hours = "0"+hours; }
		var minutes = DATE.getMinutes();
		if (minutes < 10){ minutes = "0"+minutes; }
		var timeStr = hours + ((EvenSecond) ? ":" : " ") + minutes;
		document.querySelector('#time').innerText = timeStr;

		const Days = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
		const Months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		var dateStr = Days[DATE.getDay()]+" "+DATE.getDate()+nth(DATE.getDate())+" "+Months[DATE.getMonth()]+" "+DATE.getFullYear();
		document.querySelector('#date').innerText = dateStr;

	}



	// 'Today' progress bars
	function updateTodaySection( DATE ){

		const Bookings = Array.from(document.querySelector('ul.timeline').children);
		Bookings.forEach(booking=>{
			var bookingHour = booking.getAttribute('data-hour');
			if (bookingHour <= DATE.getHours()){
				// booking has passed or is ongoing
				booking.querySelector('.tl-dot').classList.add('active');

				if (bookingHour < DATE.getHours()){
					// booking has passed, set progress bar to 100%
					booking.querySelector('.tl-progress-bar').style.height = '100%';

				}else if (bookingHour == DATE.getHours()){
					// booking is ongoing
					if (booking.classList.contains('timeline-progress')){
						// last booking of hour, set progress bar to percentage through hour
						var hourPercentage = (DATE.getMinutes()/60).toFixed(2)*100;
						booking.querySelector('.tl-progress-bar').style.height = hourPercentage+'%';
					}else{
						// not last booking of hour so set progress to 100%
						booking.querySelector('.tl-progress-bar').style.height = '100%';
					}
				}
			}
		});

	};

}();
