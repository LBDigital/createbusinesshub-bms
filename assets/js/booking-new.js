


((QuotaCheckbox, ConfirmBtn)=>{
	'use strict';

	if (QuotaCheckbox){
		QuotaCheckbox.addEventListener('change', function(){
			ConfirmBtn.disabled = !this.checked;
		});
	}


})(document.querySelector('#quotaCheckbox'), document.querySelector('#completeBtn'));



(( MONTHS )=>{
	'use strict';

	var bookingInfo = {};

	// select room
	document.querySelectorAll('.room-choose').forEach(roomImgContainer=>{
		roomImgContainer.addEventListener('click', ev=>{

			ev.currentTarget.classList.toggle('active');

			document.querySelector('#chooseTime').classList.remove('show');
			document.querySelector('#chooseUser').classList.remove('show');
			document.querySelector('#completeBooking').classList.remove('show');

			// get chosen room(s)
			var chosenRooms = document.querySelectorAll('.room-choose.active');
			if (chosenRooms.length < 1){
				// no room chosen
				delete bookingInfo.room;

				document.querySelector('#calendar').classList.remove('show');

			}else{
				// room chosen

				// if 1 room chosen, save room; else, save as room 3
				bookingInfo.room = (chosenRooms.length == 1)
					? parseInt(chosenRooms[0].dataset.room)
					: 3;

				// room chosen, show calendar
				document.querySelector('#calendar').classList.add('show');

				// scroll to calendar section
				document.querySelector('#calendar').scrollIntoView({
					behavior: 'smooth'
				});

				// update available times if date already chosen
				if (chosenDay){
					showTimes(chosenDay, calMonth, calYear);
				}

				// update complete booking btn
				var roomStr = (bookingInfo.room == 3) ? "1 & 2" : bookingInfo.room;
				document.querySelector('#confirmRoom').innerText = roomStr;

				// update hidden form input
				document.querySelector('#roomInput').value = bookingInfo.room;
			}
		});
	});



	// initialise calendar
	var calMonth = (new Date()).getMonth() +1;
	var calYear = (new Date()).getFullYear();
	var chosenDay;
	populateCalendar(calMonth, calYear);


	// 1-indexed months (1-12)
	function populateCalendar(month, year){
		// set title (month year)
		var calendarTitle = MONTHS[month-1] + " " + year;
		document.querySelector('#calendarTitle').innerText = calendarTitle;

		// populate calendar dates
		var firstDay = (new Date(year, month-1).getDay() +6)%7;
		var monthLength = new Date(year, month, 0).getDate();

		var prevMonthLength = new Date(year, month-1, 0).getDate();

		var day = 0;
		for (var rowIndex=0; rowIndex<=5; rowIndex++){
			for (var dayIndex=0; dayIndex<=6; dayIndex++){
				var calDay = document.querySelectorAll('.calendar-day')[dayIndex];
				var calDate = calDay.querySelectorAll('.calendar-date')[rowIndex];

				// clear special classes
				calDate.classList.remove('disabled');
				calDate.classList.remove('other-month');

				if (rowIndex == 0 && dayIndex < firstDay){
					// prev month
					var thisMonth = (new Date()).getMonth();
					var thisYear = (new Date()).getFullYear();

					if ( (year == thisYear) && (month-1) <= thisMonth ){
						calDate.innerHTML = "&nbsp;";
						calDate.classList.add('disabled');
					}else{
						calDate.innerText = prevMonthLength-(firstDay-1-dayIndex);
						calDate.classList.add('other-month');
						// TODO: event listener to change to prev month
							// and select chosen date



					}

				}else if (day+1 > monthLength){
					// next month
					calDate.innerText = (++day) - monthLength;
					calDate.classList.add('other-month');
					// TODO: event listener to change to next month
						// and select chosen date



				}else{
					// this month
					calDate.innerText = ++day;
					calDate.classList.remove('other-month');

				}
			}
		}
	}

	document.querySelectorAll('.calendar-date').forEach(calDate=>{
		calDate.addEventListener('click', ev=>{
			var clickedDateEl = ev.currentTarget;

			if (!clickedDateEl.classList.contains('disabled')){
				// unchose previously chosen date
				unchooseDate();

				var innerText = clickedDateEl.innerText;
				chosenDay = parseInt(innerText);

				if (clickedDateEl.classList.contains('other-month')){
					if (chosenDay > 14){
						// prev month
						prevCalMonth();

					}else{
						// next month
						nextCalMonth();
					}

					// reassign chosen date element
					document.querySelectorAll('.calendar-date').forEach(loopCalDate=>{

						if (
							!loopCalDate.classList.contains('other-month') &&
							loopCalDate.innerText == innerText
						){
							clickedDateEl = loopCalDate;
						}
					});

				}


				// highlight chosen date
				clickedDateEl.classList.add('active');

				// show available times for chosen date
				showTimes(chosenDay, calMonth, calYear);

				// update complete booking btn text
				var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
				var date = new Date(calYear, calMonth, chosenDay);
				var dayName = days[date.getDay()];
				var ordinalIndicator = (d=>{
					if (d > 3 && d < 21) return 'th';
					switch (d % 10) {
						case 1: return 'st';
						case 2: return 'nd';
						case 3: return 'rd';
						case 4: return 'th';
					}
				})(chosenDay);
				var monthStr = MONTHS[calMonth-1];
				var dateStr = `${dayName} ${chosenDay}<sup>${ordinalIndicator}</sup> ${monthStr} ${calYear}`;
				document.querySelector('#confirmDate').innerHTML = dateStr;
			}
		});
	});



	var prevMonthBtn = document.querySelector('#prevMonth');
	var nextMonthBtn = document.querySelector('#nextMonth');

	prevMonthBtn.addEventListener('click', ev=>{
		if (!ev.currentTarget.classList.contains('disabled')){
			prevCalMonth();
		}
	});
	nextMonthBtn.addEventListener('click', nextCalMonth);

	function nextCalMonth(){
		unchooseDate();

		var newCalMonth = calMonth+1;
		var newCalYear = calYear;
		if (newCalMonth > 12){
			newCalMonth = 1;
			newCalYear++;
		}

		var nowMonth = (new Date()).getMonth() +1;
		var nowYear = (new Date()).getFullYear();
		if (nowMonth != newCalMonth && nowYear != newCalYear){
			// calendar isn't current month nor year, so enable prev month btn
			prevMonthBtn.classList.remove('disabled');
		}

		populateCalendar(newCalMonth, newCalYear)
		calMonth = newCalMonth;
		calYear = newCalYear;
	}

	function prevCalMonth(){
		unchooseDate();

		var newCalMonth = calMonth-1;
		var newCalYear = calYear;
		if (newCalMonth < 1){
			newCalMonth = 12;
			newCalYear--;
		}

		var nowMonth = (new Date()).getMonth() +1;
		var nowYear = (new Date()).getFullYear();
		if (nowMonth == newCalMonth && nowYear == newCalYear){
			// calendar is current month and year, so disable prev month btn
			prevMonthBtn.classList.add('disabled');
		}

		populateCalendar(newCalMonth, newCalYear)
		calMonth = newCalMonth;
		calYear = newCalYear;
	}


	function unchooseDate(){
		// deselect previously chosen date
		document.querySelectorAll('.calendar-date')
			.forEach(d=>d.classList.remove('active'));

		chosenDay = null;
		delete bookingInfo.time;

		document.querySelector('#chooseTime').classList.remove('show');
		document.querySelector('#chooseUser').classList.remove('show');
		document.querySelector('#completeBooking').classList.remove('show');
	}


	function showTimes(day, month, year){
		// get container of available times
		var timesContainer = document.querySelector('#times');

		// clear currently shown times
		timesContainer.innerText = "";
		// show choose times section
		document.querySelector('#chooseTime').classList.add('show');

		// get bookings for chosen date
		var xhr = new XMLHttpRequest();
		var dateStr = `${day}-${month}-${year}`;
		var queryParams = `?date=${dateStr}`;
		xhr.open("GET", "../scripts/get-bookings.php"+queryParams);

		xhr.onreadystatechange = function(){
			if (this.readyState === 4){
				if (this.status === 200){
					// got bookings
					var bookings = JSON.parse(this.responseText);

					// populate available times
					var dayStartTS = new Date(year, month-1, day).getTime();
					var dayStartHourCode = Math.floor(dayStartTS/1000)/60/60;
					for (var hour=9; hour<=17; hour++){
						var hourCode = dayStartHourCode+hour;

						var timeAvail = bookings.reduce((avail, booking)=>{
							if (!avail) return false;

							if (bookingInfo['room'] == 3){
								// both rooms chosen, so only need to check time
								if (booking['time'] == hourCode){
									// booking at this time
									return false;
								}
							}else{
								// 1 room chosen, so check room and time
								if (
									(booking['time'] == hourCode) &&
									(booking['room'] == 3 || booking['room'] == bookingInfo['room'])
								){
									// chosen room booked at loop-time
									return false;
								}
							}

							return true;

						}, true);

						if (timeAvail){
							var timeEl = document.createElement('span');
							timeEl.innerText = hour+":00";
							timesContainer.appendChild(timeEl);

							(hour=>{
								timeEl.addEventListener('click', ev=>{
									document.querySelectorAll('#times span')
										.forEach(t=>t.classList.remove('active'));
									ev.currentTarget.classList.add('active');

									chooseTime(hour);
								});
							})(hour);
						}

					}


				}else{
					console.log('errrororororro!!!');
					console.log(this.responseText);

				}
			}
		}

		xhr.send();
	}



	function chooseTime(hour){
		var bookingTime = new Date(calYear, calMonth-1, chosenDay, hour);
		var hourCode = Math.floor(bookingTime.getTime()/1000)/60/60
		bookingInfo.time = hourCode;

		// update hidden form input
		document.querySelector('#timeInput').value = hourCode;

		// update complete booking btn
		document.querySelector('#confirmTime').innerText = hour+":00";

		if (document.querySelector('#chooseUserOption')){
			// admin logged in, show choose user
			document.querySelector('#chooseUser').classList.add('show');

		}else{
			// show complete booking section
			document.querySelector('#completeBooking').classList.add('show');

			// scroll to complete booking section
			document.querySelector('#completeBooking').scrollIntoView({
				behavior: 'smooth'
			});
		}

	}

	document.querySelector('#chooseUserSelect').addEventListener('change', ev=>{
		var selectedUserId = ev.target.value;
		// store selected user
		document.querySelector('#userInput').value = selectedUserId;
		// update confirm btn
		var selector = `#chooseUserSelect option[value="${selectedUserId}"]`;
		var selectedUserName = document.querySelector(selector).innerText;
		document.querySelector('#confirmUser').innerText = selectedUserName;

		// show complete booking section
		document.querySelector('#completeBooking').classList.add('show');

		// scroll to complete booking section
		document.querySelector('#completeBooking').scrollIntoView({
			behavior: 'smooth'
		});

		// user is admin; since they are choosing a user; so no quota agree necessary
		document.querySelector('#completeBtn').disabled = false;

	});


})( [
	'January','February','March','April','May','June','July','August',
	'September','October','November','December'
] );
