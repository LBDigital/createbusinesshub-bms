<?php
	require '../vendor/autoload.php';


	session_start();
	if ( !isset($_SESSION['authed']) || $_SESSION['authed'] !== true || !isset($_SESSION['admin']) || $_SESSION['admin'] !== true){
		header("location: ../login.php");
		exit;
	}

	$form_err = "";

	// check if form data has been submitted
	if ($_SERVER['REQUEST_METHOD'] == 'POST'){

		// validate inputs
		if (
			!isset($_POST['name']) ||
			!isset($_POST['email']) ||
			!isset($_POST['quota']) ||
			!isset($_POST['room']) ||

			empty(trim($_POST['name'])) ||
			empty(trim($_POST['email'])) ||
			empty(trim($_POST['quota'])) ||
			empty(trim($_POST['room']))
		){
			$form_err = "All fields are required";

		}else if ( !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) ){
			$form_err = "Invalid email given";

		}else if (!preg_match("/[A-Za-z0-9]{3}/", $_POST['name'])){
			$form_err = "Company name must contain atleast 3 alphanumeric characters";

		}else{
			// valid inputs
			$name = 	trim($_POST['name']);
			$email =	trim($_POST['email']);
			$quota =	intval(trim($_POST['quota']));
			$room =		intval(trim($_POST['room']));

			// generate username by removing special chars from company name
			$username = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $name));
			$tenant_id = "TENANT_".$username;

			$verifyCode = uniqid();

			$tenantData = [
				"id" => $tenant_id,
				"name" => $name,
				"email" => $email,
				"quota" => $quota,
				"room" => $room,

				"verify" => $verifyCode
			];

			// register tenant to db (as long as username doesn't exist)
			$putItemParams = [
				"TableName" => DB_TABLE,

				// Ensure tenant with username/email doesn't already exist
				"ConditionExpression" => "#id <> :id AND #email <> :email",
				"ExpressionAttributeNames" => [
					"#id"=>"id",
					"#email"=>"email"
				],
				"ExpressionAttributeValues" => $marshaler->marshalJson(json_encode([
					":id"=>$tenant_id,
					":email"=>$email
				])),

				"Item" => $marshaler->marshalJson(json_encode($tenantData + [
					'createdAt' => time()
				]))
			];

			try {
				$dynamoDb->putItem( $putItemParams );
				// tenant created succesfully
				$tenantCreated = true;

			}catch (Aws\DynamoDb\Exception\DynamoDbException $e) {
				if (
					array_key_exists("message", $e->toArray()) &&
					$e->toArray()["message"] == "The conditional request failed"
				){
					$form_err = "A user with the username '{$username}' or email '{$email}' already exists!";
				}else{
					// echo "Unable to put item:\n";
					// echo $e->getMessage();
					$form_err = "An unknown error occured, please contact a system admin";
				}
			}

			if (isset($tenantCreated)){
				// log action
				logger( 'TENANT_CREATE', $_SESSION['id'],  $tenantData);

				// send email to tenant
				$emailParams = new \SendGrid\Mail\Mail();
				$emailParams->setFrom("verify@createbusinesshub.com", "Create Business Hub");
				$emailParams->setSubject("Finalise Your Account Setup");
				$emailParams->addTo($email, $name);

				$verifyLink = BASE_DIR."/auth/verify.php?v=".$verifyCode;
				$emailParams->addContent("text/plain",
					"Welcome to Create Business Hub's meeting room booking system.  Click the following link to finalise your account setup and get started right away: {$verifyLink}"
				);
				// $email->addContent("text/html", "");

				$sendgrid = new \SendGrid( SENDGRID_API_KEY );
				try {
					$response = $sendgrid->send( $emailParams );
				} catch (Exception $e) {
					// echo "Caught Exception: ". $e->getMessage() ."\n";
				}


			}

		}

	}

 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<?php include '../partials/head.php' ?>
	</head>
	<body>

		<header>
			<?php include '../partials/header.php'; ?>
		</header>


		<main class="pt-4">

			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-8 col-lg-4">

						<div class="bg-light p-4">
							<h2 class="mb-4 text-center font-weight-bold">New Tenant</h2>

							<?php if(!isset($tenantCreated)){ ?>
								<form action="" method="post" class="">
									<div class="form-group">
										<label for="companyInput">Company Name</label>
										<input type="text" name="name" class="form-control" id="companyInput" required autofocus>
										<small class="form-text text-muted">This will be used to create their login username</small>
									</div>
									<div class="form-group">
										<label for="emailInput">Tenant Email</label>
										<input type="email" name="email" class="form-control" id="emailInput" required>
										<small class="form-text text-muted">Used to confirm bookings and general contact</small>
									</div>
									<div class="form-group">
										<label for="quotaInput">Monthly Bookings Quota</label>
										<input type="number" name="quota" class="form-control" id="quotaInput" value="6" required>
										<small class="form-text text-muted">Amount of free booking hours they're allowed each month</small>
									</div>
									<div class="form-group">
										<label for="roomInput">Tenants Room Number</label>
										<input type="number" name="room" class="form-control" id="roomInput" required>
										<small class="form-text text-muted">Their room number in CREATE</small>
									</div>

									<button class="btn btn-primary w-100">Register Tenant</button>

									<small class="text-danger d-block mt-2"><?php echo $form_err; ?></small>

								</form>
							<?php }else{ ?>
								<p>
									Tenant
									<strong><?php echo $name; ?></strong>
									created succesfully with username
									<strong><?php echo $username; ?></strong>
									and an email has been sent to
									<strong><?php echo $email; ?></strong>
									for them to finalise their account.
								</p>

								<a href="<?php echo BASE_DIR; ?>/">
									<button class="btn btn-primary mt-4 w-100">Return to Admin Home</button>
								</a>
							<?php } ?>
						</div>

					</div>
				</div>
			</div>

		</main>


		<footer>
			<?php include '../partials/footer.php'; ?>
		</footer>


	</body>
</html>


<!--
TODO:
	- persistant sessions (just store session id in db with their user record??  - have a google, see whats simplest)
	- footer with lb.digital link NEED TO MAKE SURE LB DIGITAL IS SOMETHING BY THEN
 -->
