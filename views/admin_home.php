


<div class="container-fluid">

	<!-- PRE CONTENT -->
	<div class="row px-5 py-1 mb-4 sub-header">
		<div class="col">
			<span id="time"></span><span id="date"></span>
		</div>
	</div>



	<!-- MAIN CONTENT -->
	<div class="row px-5">
		<div class="col-8">

			<div class="row">

				<div class="col-6">
					<div class="home-module module-qa">
						<div class="module-header border-0x">
							<p>Quick Actions</p>
						</div>
						<div class="module-body">
							<div class="qa-color-block"></div>

							<div class="quick-actions">
								<a href="#" class="quick-action">
									<div class="quick-action-body">
										<i class="far fa-id-badge"></i>
										<p>Guest Booking</p>
										<small class="text-muted">Book a meeting room on behalf of a guest.</small>
									</div>
								</a>
								<a href="tenant/new.php" class="quick-action">
									<div class="quick-action-body">
										<i class="fas fa-user-plus"></i>
										<p>New Tenant</p>
										<small class="text-muted">Register a new tenant to the system.</small>
									</div>
								</a>
								<a href="booking/new.php" class="quick-action">
									<div class="quick-action-body">
										<i class="far fa-calendar-plus"></i>
										<p>Room Booking</p>
										<small class="text-muted">Book a meeting room on behalf of a tenant / for management.</small>
									</div>
								</a>
								<a href="#" class="quick-action">
									<div class="quick-action-body">
										<i class="fas fa-user-minus"></i>
										<p>Remove Tenant</p>
										<small class="text-muted">Remove a tenant from the system.</small>
									</div>
								</a>
							</div>

						</div>
					</div>
				</div>

				<div class="col-6">

					<div class="home-module">
						<div class="module-header">
							<p>Today</p>
						</div>
						<div class="module-body">

							<ul class="timeline">

								<?php
									// Load todays bookings from DB

									// Define 2 timezones for UTC and London
									$dateTimeZoneLondon = new DateTimeZone('Europe/London');
									$dateTimeZoneUTC = new DateTimeZone('UTC');
									// Get the time 'now' at UTC and London
									$dateTimeLondon = new DateTime("now", $dateTimeZoneLondon);
									$dateTimeUTC = new DateTime("now", $dateTimeZoneUTC);
									// Get the offset between the 2 times
									$secondsOffset = $dateTimeZoneLondon->getOffset($dateTimeUTC);
									$hoursOffset = $secondsOffset/60/60;


									$dayStart = (floor(time()/60/60/24)*24) - $hoursOffset; // unix hours at 00:00 today in London
									$dayEnd = $dayStart+23; // unix hours at 23:00 today in London

									$eav = $marshaler->marshalJson(json_encode([
										':b' => 'BOOKING_',
										':startTime' => $dayStart,
										':endTime' => $dayEnd
									]));

									$params = [
										'TableName' => DB_TABLE,
										'FilterExpression' => 'begins_with(#id, :b) AND #time BETWEEN :startTime AND :endTime',
										'ExpressionAttributeNames' => [ '#id'=>'id', '#time'=>'time' ],
										'ExpressionAttributeValues' => $eav
									];

									try{
										$result = $dynamoDb->scan($params);

									}catch (DynamoDbException $e){
										echo "Unable to scan:\n";
										echo $e->getMessage() . "\n";
									}

									if ($result['Count'] < 1){
										echo "NO BOOKINGS FOR TODAY!";
										// TODO: make this show a better message, or something else.
									}else{

										// load user (tenant/admin) names of each booking
										$bookings = [];
										$users = [];
										$filterExp = "#id IN (";
										$count = 0;
										foreach($result['Items'] as $booking){
											$booking = $marshaler->unmarshalItem($booking);
											// convert hour timestamp to time format
											$unixTime = $booking['time']*60*60;
											$booking['hour'] = date('G', $unixTime) + $hoursOffset;
											// store unmarshalled item in array of bookings
											$bookings[ $booking['id'] ] = $booking;

											if (!array_key_exists($booking['user'], $users)){
												$users[':user' . ++$count] = $booking['user'];
												$filterExp .= ":user".$count.", ";
											}
										}

										$filterExp = substr($filterExp, 0, strlen($filterExp)-2) . ")";
										$eav = $marshaler->marshalJson(json_encode( $users ));

										$params = [
											'TableName' => $_TABLE_NAME,
											'FilterExpression' => $filterExp,
											'ExpressionAttributeNames' => [ '#id'=>'id' ],
											'ExpressionAttributeValues' => $eav
										];
										// var_dump($params);

										try{
											$result = $dynamoDb->scan($params);
										}catch (DynamoDbException $e){
											echo "Unable to scan:\n";
											echo $e->getMessage() . "\n";
										}
										// add users Name to each booking
											// store in timed bookings arr (bookings with key as 24 hour)
										$hourKeyBookings = [];
										foreach($result['Items'] as $user){
											$user = $marshaler->unmarshalItem($user);
											foreach($bookings as $bookingId => $booking){
												if ($booking['user'] == $user['id']){
													$booking['usersName'] = $user['name'];

													if (!isset($hourKeyBookings[ $booking['hour'] ])){
														$hourKeyBookings[ $booking['hour'] ] = [];
													}
													array_push($hourKeyBookings[ $booking['hour'] ], $booking);
												}
											}
										}
										ksort($hourKeyBookings, SORT_NUMERIC);

										$allBookingTimes = array_keys($hourKeyBookings);
										$latestBookingTime = end($allBookingTimes);

										// foreach set of hourly bookings
										foreach ($hourKeyBookings as $hour => $bookings){
											$bookingsThisHour = count($bookings);

											// there's a time gap between this booking and the next && it's not the last booking of today
											$gapAfter = ( !isset($hourKeyBookings[$hour+1]) && $latestBookingTime !== $hour );

											// foreach booking during this hour
											foreach ($bookings as $index => $booking){
												$room = ($booking['room'] < 3 ? "Room ".$booking['room'] : "Rooms 1 & 2");
												?>

												<li
													class="<?php if ($index+1 == $bookingsThisHour){
														// is last booking of hour, so setup hour progress bar
														echo ' timeline-progress ';
														// time gap after booking, so add space
														if ($gapAfter){ echo ' tl-gap-after '; }
													} ?>"
													data-hour="<?php echo $hour; ?>"
												>
													<p><?php if ($index == 0){ echo $hour.":00"; } ?></p>
													<div>
														<div class="tl-dot"></div>
														<div class="tl-progress-container">
															<div class="tl-progress-bar"></div>
														</div>
													</div>
													<p>
														<?php echo $booking['usersName']; ?>
														<?php if (substr($booking['user'], 0, 5) === "GUEST"){ ?>
															<span class="tl-tag">Guest</span>
														<?php } ?>
														<span class="tl-tag"><?php echo $room; ?></span>
													</p>
												</li>

											<?php }
										}

									}

								?>
							</ul>


						</div>
					</div>

				</div>
			</div>

		</div>
		<div class="col-4">

			<div class="home-module">
				<div class="module-header">
					<p>Recent Activity</p>
				</div>
				<div class="module-body">
					bbbbb
				</div>
			</div>

		</div>
	</div>
</div>

<script src="./assets/js/admin_home.js" charset="utf-8"></script>



<!--
HOME
1. add new tenant
2. delete old tenant
3. view all tenants (searchable)
4. book room
5. book room for tenant

-->
