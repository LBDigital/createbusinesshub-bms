<?php
	require './vendor/autoload.php';

	session_start();
	if ( !isset($_SESSION['authed']) || $_SESSION['authed'] !== true ){
		header("location: ./auth/login.php");
		exit;
	}


 ?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
	<head>
		<?php include './partials/head.php' ?>
	</head>
	<body>

		<header>
			<?php include './partials/header.php'; ?>
		</header>


		<main>

			<?php
				if ( isset($_SESSION['admin']) && $_SESSION['admin'] === true ){
					include './views/admin_home.php';
				}
			?>

		</main>


		<footer>
			<?php include './partials/footer.php'; ?>
		</footer>


	</body>
</html>


<!--
TODO:
	- persistant sessions (just store session id in db with their user record??  - have a google, see whats simplest)
	- footer with lb.digital link NEED TO MAKE SURE LB DIGITAL IS SOMETHING BY THEN
	- on new tenant add, tenant recieves email welcoming them and telling them to login to set a password
	- admin panel: recent activity feed.  Includes smart info such as "ReelShare cancelled meeting room 1 for Thursday 5th November at 10pm.  This is their "
	- recent activity polls every minute when admin page is open, checking for new activity
	- clocks changing support (store time to database based on the time it will be on that date, not todays time??)
	- update today schedule every min? - do it in JS, with an ajax call to the local PHP file
	- clickable 'today' schedule items, for example a guest booking will show the guests details
	- Authenticate domain with SendGrid to improve deliverability
 -->
