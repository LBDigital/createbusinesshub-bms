

<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-6 px-5">
	<div class="col d-flex">
		<a class="navbar-brand" href="<?php echo BASE_DIR; ?>/">CREATE Bookings</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<span class="navbar-text text-light ml-auto"><?php if ( isset($_SESSION['authed']) && $_SESSION['authed'] === true ){ echo $_SESSION['name']; } ?></span>
			<ul class="navbar-nav ml-4">
				<!-- <li class="nav-item">
					<a href="#" class="nav-link">Home</a>
				</li> -->
				<li class="nav-item">
					<!-- <a href="./login.php" class="nav-link">Login</a> -->
					<?php if ( isset($_SESSION['authed']) && $_SESSION['authed'] === true ){ ?>
						<a href="<?php echo BASE_DIR; ?>/auth/logout.php" class="nav-link">Logout</a>
					<?php } ?>
				</li>
				<!-- <li class="nav-item">
					<a href="#" class="nav-link">My Bookings</a>
				</li> -->
			</ul>
		</div>
	</div>

</nav>
