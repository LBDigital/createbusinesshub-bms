<?php

// autoloaded by composer
	// use $GLOBALS to access constants defined in other autoloaded scripts
	// e.g: $GLOBALS['marshaler'], not $marshaler



// log activity to DB
function logger($action, $performedBy, $data){

	$putItemParams = [
		"TableName" => DB_LOGS_TABLE,
		"Item" => $GLOBALS['marshaler']->marshalJson(json_encode([
			'id' => uniqid(),

			'action' => $action,
			'createdAt' => time(),

			'performedBy' => $performedBy,
			'data' => $data
		]))
	];

	try {
		$GLOBALS['dynamoDb']->putItem( $putItemParams );

	}catch (Aws\DynamoDb\Exception\DynamoDbException $e) {
		echo "Unable to log action:\n";
		echo $e->getMessage();
	}

}







?>
