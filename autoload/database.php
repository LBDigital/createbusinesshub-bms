<?php



use Aws\DynamoDb\Exception\DynamoDbException;
use Aws\DynamoDb\Marshaler;

$aws = new Aws\Sdk([
	'profile'	=> 'lbdigital',
	'region'	=> 'eu-west-2', // EU (London)
	'version'	=> 'latest'
]);

$dynamoDb = $aws->createDynamoDb();
$marshaler = new Marshaler();


// set globals
$GLOBALS['aws'] = $aws;
$GLOBALS['dynamoDb'] = $dynamoDb;
$GLOBALS['marshaler'] = $marshaler;


?>
